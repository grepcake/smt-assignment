// TODO remove SSA for prettiness?..
fun pprintTree(tree: ExecTreeNode): String =
    breakIntoPaths(tree)
        .joinToString(separator = "\n${"-".repeat(10)}\n") { pprintPath(it) }

class ExecPathNode(
    val child: ExecPathNode?,
    val nextStmt: Stmt,
    val sStore: List<Let>,
    val cStore: List<Expr>
)

// TODO tail recursion
// TODO at most one assert per path?
fun breakIntoPaths(tree: ExecTreeNode): Sequence<ExecPathNode> = when {
    tree.children.isEmpty() ->
        sequenceOf(ExecPathNode(null, tree.nextStmt, tree.symbolicStore, tree.constraintStore))

    else -> tree.children.asSequence()
        .flatMap { breakIntoPaths(it) }
        .map { ExecPathNode(it, tree.nextStmt, tree.symbolicStore, tree.constraintStore) }
}

tailrec fun pprintPath(
    path: ExecPathNode?, data: MutableList<String> = mutableListOf()
): String = when {
    path == null -> data.joinToString(separator = "\n   ↓\n") { it }
    else -> {
        data.add(buildString {
            append(pprintSStore(path.sStore)); append("\n")
            append(pprintCStore(path.cStore)); append("\n")
            append(pprintStmtHead(path.nextStmt))
        })
        pprintPath(path.child, data)
    }
}

// TODO keep only the latest reassignment
fun pprintSStore(sStore: List<Let>): String =
    "σ = {${sStore.joinToString { pprintLet(it) }}}"

fun pprintCStore(cStore: List<Expr>): String =
    cStore.joinToString(separator = "∧", prefix = "π = {", postfix = "}")
    { pprintExpr(it) }

fun pprintStmtHead(stmt: Stmt): String = when (stmt) {
    is Assert -> "assert ${pprintExpr(stmt.cond)}"
    is Block -> throw AssertionError("Nodes shouldn't contain Blocks, only granular statements")
    is If -> "if (${pprintExpr(stmt.cond)}) …"
    is Let -> "var ${stmt.variable.name} = ${pprintExpr(stmt.value)}"
}

fun pprintLet(let: Let): String =
    "${let.variable.name}↦${pprintExpr(let.value)}"

fun pprintExpr(expr: Expr): String = when (expr) {
    is Const -> "${expr.value}"
    is Minus -> "(${pprintExpr(expr.left)} - ${pprintExpr(expr.right)})"
    is Mul -> "(${pprintExpr(expr.left)} * ${pprintExpr(expr.right)})"
    is Plus -> "(${pprintExpr(expr.left)} + ${pprintExpr(expr.right)})"
    is Var -> expr.name
    is Eq -> "(${pprintExpr(expr.left)} == ${pprintExpr(expr.right)})"
    is NEq -> "(${pprintExpr(expr.left)} != ${pprintExpr(expr.right)})"
    is Ge -> "(${pprintExpr(expr.left)} >= ${pprintExpr(expr.right)})"
    is Gt -> "(${pprintExpr(expr.left)} > ${pprintExpr(expr.right)})"
}

fun pprintAssertState(assertState: AssertState): String {
    val breadcrumbsStr = assertState.breadcrumbs.joinToString("\n") { pprintStmtHead(it) }
    val sStoreStr = pprintSStore(assertState.sStore)
    val cStoreStr = pprintCStore(assertState.cStore)
    val assertionStr = pprintExpr(assertState.assertion)
    // have to use non-indented text, because interpolated strings mess up the indent
    return """
Assert State:
Breadcrumbs:
$breadcrumbsStr

Symbolic Store:
$sStoreStr

Constraint Store:
$cStoreStr

Assertion:
$assertionStr
""".trimIndent()
}

fun pprintCheckResult(checkResult: CheckResult): String {
    return when (checkResult) {
        is CheckResult.Fail -> {
            """
                Check Result: FAIL
                Models are non-deterministic, so not suitable for tests.
            """.trimIndent()
        }

        is CheckResult.Ok -> "Check Result: OK"
        is CheckResult.Unknown -> {
            val reasonStr = checkResult.reason
            """
                Check Result: UNKNOWN
                Reason: $reasonStr
            """.trimIndent()
        }
    }
}

fun pprintResult(results: List<Pair<AssertState, CheckResult>>): String {
    return results
        .map { pprintAssertState(it.first) to pprintCheckResult(it.second) }
        .sortedBy { it.first }
        .joinToString("\n\n") { result ->
            val assertStateStr = result.first
            val checkResultStr = result.second
            """
==========================================
$assertStateStr

------------------------------------------
$checkResultStr

==========================================
""".trimIndent()
        }
}
