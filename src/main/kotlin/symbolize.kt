// Instead of using SymVal, we will simply interpret
// all Vars as symbolic values

class ExecTreeNode(
    val children: List<ExecTreeNode>,
    val nextStmt: Stmt,
    // a persistent hash-map would work better, but we're keeping it simple
    val symbolicStore: List<Let>,
    val constraintStore: List<BoolExpr>
)

fun symbolize(stmt: Stmt) = symbolizeAux(stmt, emptyList(), emptyList(), emptyMap(), emptyList())

// The 'counter' argument is necessary to bring each branch
// into a static single assignment form (SSA).
fun symbolizeAux(
    stmt: Stmt,
    sStore: List<Let>,
    cStore: List<BoolExpr>,
    counter: Map<String, Int>,
    cont: List<Stmt>
): ExecTreeNode? = when (stmt) {
    is Block -> when {
        stmt.stmts.isEmpty() && cont.isEmpty() -> null
        stmt.stmts.isEmpty() -> symbolizeCont(cont, sStore, cStore, counter)
        else -> {
            // persistent linked lists would work better from the standpoint
            // of both memory and complexity, but alas
            val head = stmt.stmts.first()
            val tail = stmt.stmts.drop(1)
            symbolizeAux(head, sStore, cStore, counter, tail + cont)
        }
    }

    is Let -> {
        val newValue = Substituter(counter).actOn(stmt.value)
        val varName = stmt.variable.name
        val newCounter = counter + (varName to counter.getOrDefault(varName, 0) + 1)
        val newStmt = Let(Substituter(newCounter).actOnVar(stmt.variable), newValue)
        val newSStore = sStore + newStmt
        val child = symbolizeCont(cont, newSStore, cStore, newCounter)
        ExecTreeNode(listOfNotNull(child), newStmt, sStore, cStore)
    }

    is If -> {
        // the number of segments is exponential in the number of Ifs,
        // but we're keeping it simple
        val thenCStore = cStore + Substituter(counter).actOn(stmt.cond)
        val elseCStore = cStore + Substituter(counter).actOn(negateBoolExpr(stmt.cond))
        val child1 = symbolizeAux(stmt.thenStmt, sStore, thenCStore, counter, cont)
        val child2 = symbolizeAux(stmt.elseStmt ?: Block(), sStore, elseCStore, counter, cont)
        val newStmt = Substituter(counter).actOn(stmt)
        ExecTreeNode(listOfNotNull(child1, child2), newStmt, sStore, cStore)
    }

    is Assert -> {
        val child = symbolizeCont(cont, sStore, cStore, counter)
        ExecTreeNode(listOfNotNull(child), Substituter(counter).actOn(stmt), sStore, cStore)
    }
}

fun symbolizeCont(
    cont: List<Stmt>,
    sStore: List<Let>,
    cStore: List<BoolExpr>,
    counter: Map<String, Int>,
) = symbolizeAux(Block(*cont.toTypedArray()), sStore, cStore, counter, emptyList())

fun negateBoolExpr(expr: BoolExpr) = when (expr) {
    is Eq -> NEq(expr.left, expr.right)
    is NEq -> Eq(expr.left, expr.right)
    is Ge -> Gt(expr.right, expr.left)
    is Gt -> Ge(expr.right, expr.left)
}

