
import org.ksmt.KContext
import org.ksmt.expr.KExpr
import org.ksmt.solver.KModel
import org.ksmt.solver.KSolverStatus
import org.ksmt.solver.cvc5.KCvc5Solver
import org.ksmt.sort.KBoolSort
import org.ksmt.sort.KIntSort
import org.ksmt.utils.mkConst
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.time.Duration.Companion.minutes

fun check(tree: ExecTreeNode): List<Pair<AssertState, CheckResult>> {
    Executors.newFixedThreadPool(8).use {
        val futures = mutableListOf<Pair<AssertState, Future<CheckResult>>>()
        for (state in findAssertStates(tree)) {
            futures.add(state to it.submit<CheckResult> { checkAssertState(state) })
        }
        return futures.map { it.first to it.second.get() }
    }
}

class AssertState(
    val breadcrumbs: List<Stmt>,
    val sStore: List<Let>,
    val cStore: List<BoolExpr>,
    val assertion: BoolExpr
)

fun findAssertStates(
    tree: ExecTreeNode,
    breadcrumbs: List<Stmt> = emptyList()
): Iterator<AssertState> = iterator {
    val newBreadcrumbs = breadcrumbs + tree.nextStmt
    if (tree.nextStmt is Assert) {
        yield(
            AssertState(
                newBreadcrumbs,
                tree.symbolicStore,
                tree.constraintStore,
                tree.nextStmt.cond
            )
        )
    }
    tree.children.forEach { yieldAll(findAssertStates(it, newBreadcrumbs)) }
}

class Smtizer(val ctx: KContext, val smtVars: Map<String, KExpr<KIntSort>>) {
    fun actOn(expr: BoolExpr): KExpr<KBoolSort> = when (expr) {
        is Eq -> ctx.mkEq(actOn(expr.left), actOn(expr.right))
        is NEq -> ctx.mkDistinct(listOf(actOn(expr.left), actOn(expr.right)))
        is Ge -> ctx.mkArithGe(actOn(expr.left), actOn(expr.right))
        is Gt -> ctx.mkArithGt(actOn(expr.left), actOn(expr.right))
    }

    fun actOn(expr: ArithExpr): KExpr<KIntSort> = when (expr) {
        is Const -> ctx.mkIntNum(expr.value)
        is Minus -> ctx.mkArithSub(actOn(expr.left), actOn(expr.right))
        is Mul -> ctx.mkArithMul(actOn(expr.left), actOn(expr.right))
        is Plus -> ctx.mkArithAdd(actOn(expr.left), actOn(expr.right))
        is Var -> smtVars.getValue(expr.name)
    }
}

sealed class CheckResult {
    data class Fail(val model: KModel) : CheckResult()
    object Ok : CheckResult()
    data class Unknown(val reason: String) : CheckResult()
}

// Assuming SSA
// We also let the solver pick the logic themselves,
// they can generally pick an optimal option.
fun checkAssertState(state: AssertState): CheckResult = with(KContext()) {
    val smtVars = gatherVars(state).associateWith { intSort.mkConst(it) }
    val smtizer = Smtizer(this, smtVars)
    val solver = KCvc5Solver(this) // TODO configuration
    state.sStore.forEach {
        solver.assert(smtizer.actOn(Eq(it.variable, it.value)))
    }
    state.cStore.forEach { solver.assert(smtizer.actOn(it)) }
    // The assertion holds if there is no such assignment of
    // variables that would falsify it. Thus, we falsify and
    // check for SAT.
    solver.assert(mkNot(smtizer.actOn(state.assertion)))
    return when (solver.check(1.minutes)) {
        KSolverStatus.SAT -> CheckResult.Fail(solver.model())
        KSolverStatus.UNSAT -> CheckResult.Ok
        KSolverStatus.UNKNOWN -> CheckResult.Unknown(solver.reasonOfUnknown())
    }
}