sealed class Stmt
class Block(vararg val stmts: Stmt) : Stmt()
class Let(val variable: Var, val value: ArithExpr) : Stmt()
class If(val cond: BoolExpr, val thenStmt: Stmt, val elseStmt: Stmt? = null) : Stmt()
class Assert(val cond: BoolExpr) : Stmt()

sealed class Expr
sealed class BoolExpr : Expr()
class Eq(val left: ArithExpr, val right: ArithExpr) : BoolExpr()
class NEq(val left: ArithExpr, val right: ArithExpr) : BoolExpr()
class Gt(val left: ArithExpr, val right: ArithExpr) : BoolExpr()
class Ge(val left: ArithExpr, val right: ArithExpr) : BoolExpr()


sealed class ArithExpr : Expr()
class Const(val value: Int) : ArithExpr()
class Var(val name: String) : ArithExpr()
class Plus(val left: ArithExpr, val right: ArithExpr) : ArithExpr()
class Minus(val left: ArithExpr, val right: ArithExpr) : ArithExpr()
class Mul(val left: ArithExpr, val right: ArithExpr) : ArithExpr()
