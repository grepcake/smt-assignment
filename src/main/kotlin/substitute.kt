class Substituter(val counter: Map<String, Int>) {
    // We only care about the condition, because statements in branches
    // are processed on their own in tree branches/paths.
    fun actOn(stmt: If): Stmt =
        If(actOn(stmt.cond), stmt.thenStmt, stmt.elseStmt)

    fun actOn(stmt: Assert): Assert =
        Assert(actOn(stmt.cond))

    fun actOn(expr: Expr): Expr = when (expr) {
        is BoolExpr -> actOn(expr)
        is ArithExpr -> actOn(expr)
    }

    fun actOn(expr: ArithExpr): ArithExpr = when (expr) {
        is Const -> expr
        is Minus -> Minus(actOn(expr.left), actOn(expr.right))
        is Mul -> Mul(actOn(expr.left), actOn(expr.right))
        is Plus -> Plus(actOn(expr.left), actOn(expr.right))
        is Var -> actOnVar(expr)
    }

    fun actOn(expr: BoolExpr): BoolExpr = when (expr) {
        is Eq -> Eq(actOn(expr.left), actOn(expr.right))
        is NEq -> NEq(actOn(expr.left), actOn(expr.right))
        is Ge -> Ge(actOn(expr.left), actOn(expr.right))
        is Gt -> Gt(actOn(expr.left), actOn(expr.right))
    }

    fun actOnVar(v: Var): Var =
        Var("${v.name}!${counter.getOrDefault(v.name, 0)}")
}
