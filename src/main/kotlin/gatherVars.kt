fun gatherVars(state: AssertState): List<String> =
    state.breadcrumbs.flatMap { gatherVars(it) } +
            state.cStore.flatMap { gatherVars(it) } +
            state.sStore.flatMap { gatherVars(it) } +
            gatherVars(state.assertion)

fun gatherVars(stmt: Stmt): List<String> = when (stmt) {
    is Assert -> gatherVars(stmt.cond)
    is Block -> stmt.stmts.flatMap { gatherVars(it) }
    is If -> gatherVars(stmt.cond)
    is Let -> listOf(stmt.variable.name) + gatherVars(stmt.value)
}

fun gatherVars(expr: Expr): List<String> = when (expr) {
    is Const -> emptyList()
    is Minus -> gatherVars(expr.left) + gatherVars(expr.right)
    is Mul -> gatherVars(expr.left) + gatherVars(expr.right)
    is Plus -> gatherVars(expr.left) + gatherVars(expr.right)
    is Var -> listOf(expr.name)
    is Eq -> gatherVars(expr.left) + gatherVars(expr.right)
    is NEq -> gatherVars(expr.left) + gatherVars(expr.right)
    is Ge -> gatherVars(expr.left) + gatherVars(expr.right)
    is Gt -> gatherVars(expr.left) + gatherVars(expr.right)
}

