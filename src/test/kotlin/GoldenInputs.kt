/*
    var x = 1
    var y = 0
    if (a != 0) {
        y = 3 + x
        if (b == 0) {
            x = 2 * (a + b)
        }
    }
    assert (x - y) != 0
 */
val fooBarAst = Block(
    Let(Var("x"), Const(1)),
    Let(Var("y"), Const(0)),
    If(
        NEq(Var("a"), Const(0)),
        Block(
            Let(Var("y"), Plus(Const(3), Var("x"))),
            If(
                Eq(Var("b"), Const(0)),
                Let(Var("x"), Mul(Const(2), Plus(Var("a"), Var("b")))),
            )
        )
    ),
    Assert(NEq(Minus(Var("x"), Var("y")), Const(0)))
)

val ifAst = Block(
    If(
        Eq(Var("a"), Const(0)),
        Let(Var("z"), Const(0)),
        Let(Var("z"), Const(1))
    ),
    Assert(Eq(Var("z"), Const(0)))
)

// Note, that assert is only in one branch, so the tree has only one branch as well.
val changedCondAst = Block(
    If(
        Eq(Var("x"), Const(0)),
        Block(
            Let(Var("x"), Const(5)),
            Assert(NEq(Var("x"), Const(5)))
        ),
    )
)

val badMaxAst = Block(
    If (
        Gt(Var("a"), Var("b")),
        Let(Var("m"), Var("a"))
    ),
    If (
        Gt(Var("b"), Var("a")),
        Let(Var("m"), Var("b"))
    ),
    Assert(Ge(Var("m"), Var("a"))),
    Assert(Ge(Var("m"), Var("b"))),
)

val goodMaxAst = Block(
    If (
        Ge(Var("a"), Var("b")),
        Let(Var("m"), Var("a"))
    ),
    If (
        Ge(Var("b"), Var("a")),
        Let(Var("m"), Var("b"))
    ),
    Assert(Ge(Var("m"), Var("a"))),
    Assert(Ge(Var("m"), Var("b"))),
)