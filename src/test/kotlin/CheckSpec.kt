import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec

// TODO deterministic SMT-checking to enable models?
class CheckSpec : StringSpec() {
    init {
        "foobar" {
            val actual = pprintResult(check(symbolize(fooBarAst)!!))
            val golden = SymbolizeSpec::class.java.getResource("/check/foobar.golden")!!.readText()
            actual shouldBe golden
        }
        "if" {
            val actual = pprintResult(check(symbolize(ifAst)!!))
            val golden = SymbolizeSpec::class.java.getResource("/check/if.golden")!!.readText()
            actual shouldBe golden
        }
        "changedCond" {
            val actual = pprintResult(check(symbolize(changedCondAst)!!))
            val golden = SymbolizeSpec::class.java.getResource("/check/changedCond.golden")!!.readText()
            actual shouldBe golden
        }
        "badMax" {
            val actual = pprintResult(check(symbolize(badMaxAst)!!))
            val golden = SymbolizeSpec::class.java.getResource("/check/badMax.golden")!!.readText()
            actual shouldBe golden
        }
        "goodMax" {
            val actual = pprintResult(check(symbolize(goodMaxAst)!!))
            val golden = SymbolizeSpec::class.java.getResource("/check/goodMax.golden")!!.readText()
            actual shouldBe golden
        }
    }
}

