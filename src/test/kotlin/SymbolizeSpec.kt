// Usually I use bats (bash-based) for golden testing,
// and automatically generate tests based on «golden»-files,
// but I suspect the IDEA build system to be too finicky.

// TODO parse input files

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec

class SymbolizeSpec : StringSpec() {
    init {
        "foobar" {
            val actual = pprintTree(symbolize(fooBarAst)!!)
            val golden = SymbolizeSpec::class.java.getResource("/symbolize/foobar.golden")!!.readText()
            actual shouldBe golden
        }
        "if" {
            val actual = pprintTree(symbolize(ifAst)!!)
            val golden = SymbolizeSpec::class.java.getResource("/symbolize/if.golden")!!.readText()
            actual shouldBe golden
        }
        "changedCond" {
            val actual = pprintTree(symbolize(changedCondAst)!!)
            val golden = SymbolizeSpec::class.java.getResource("/symbolize/changedCond.golden")!!.readText()
            actual shouldBe golden
        }
    }
}

